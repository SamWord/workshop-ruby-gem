# frozen_string_literal: true

require_relative "workshop/version"

module Workshop
  class Error < StandardError; end

  def self.hi
    puts "Bonjour tout le monde!"
  end
end
